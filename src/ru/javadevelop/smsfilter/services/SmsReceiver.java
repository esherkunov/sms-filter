package ru.javadevelop.smsfilter.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.activities.AddRuleActivity;
import ru.javadevelop.smsfilter.common.ExtApplication;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;
import ru.javadevelop.smsfilter.model.filter.FilterUtils;
import ru.javadevelop.smsfilter.model.filter.WhiteList;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 20.04.13
 * Time: 9:26
 * To change this template use File | Settings | File Templates.
 */
public class SmsReceiver extends BroadcastReceiver {

    public static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    public static final String TAG = "br_sms_test";

    public void onReceive(Context context, Intent intent) {
        if(intent != null && intent.getAction() != null && intent.getAction().compareToIgnoreCase(ACTION) == 0) {
            Object[] pdus = (Object[])intent.getExtras().get("pdus");
            SmsMessage[] smsMessages = new SmsMessage[pdus.length];
            for(int i = 0; i < pdus.length; i++) {
                smsMessages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
            }
            StringBuilder sb = new StringBuilder();
            for(SmsMessage smsMessage: smsMessages) {
                sb.append(smsMessage.getMessageBody());
            }

            String sender = smsMessages[0].getOriginatingAddress();
            String body = sb.toString();
            
            Log.d(TAG, "sender: " + sender + "\n" + "body: " + body);
            
	        if(WhiteList.isInWhiteList(context, sender, null)) return;
	        
	        BlackListFilter filter;
	        if(FilterUtils.isInContactList(context, sender)) {
		        filter = BlackListFilter.isInBlackList(context, sender, null);
	        } else {
		        filter = BlackListFilter.isInBlackList(context, sender, body);
	        }
	        if(filter != null) {
	        	filter.executeAction();
	        } else {
		        sendNotification(context, sender, body);
	        }

	        this.abortBroadcast();
        }
    }

	public void sendNotification(Context context, String title, String text) {
		
//		TODO Мне кажется такое поведение нужно переделать На интенты. Всё-таки это не android подход
		ExtApplication.Session.sms.text = text;
		ExtApplication.Session.sms.phoneNumber = title;

		NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(context)
				.setContentTitle(title)
				.setContentText(text)
				.setSmallIcon(R.drawable.icon)
				.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, AddRuleActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
		NotificationManager notifyManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notify = notifyBuilder.build();
		notify.flags |= Notification.FLAG_AUTO_CANCEL;
		notifyManager.notify(1, notify);
	}

    /*private static void createFakeSms(Context context, String sender,
                                      String body) {
        byte[] pdu = null;
        byte[] scBytes = PhoneNumberUtils
                .networkPortionToCalledPartyBCD("0000000000");
        byte[] senderBytes = PhoneNumberUtils
                .networkPortionToCalledPartyBCD(sender);
        int lsmcs = scBytes.length;
        byte[] dateBytes = new byte[7];
        Calendar calendar = new GregorianCalendar();
        dateBytes[0] = reverseByte((byte) (calendar.get(Calendar.YEAR)));
        dateBytes[1] = reverseByte((byte) (calendar.get(Calendar.MONTH) + 1));
        dateBytes[2] = reverseByte((byte) (calendar.get(Calendar.DAY_OF_MONTH)));
        dateBytes[3] = reverseByte((byte) (calendar.get(Calendar.HOUR_OF_DAY)));
        dateBytes[4] = reverseByte((byte) (calendar.get(Calendar.MINUTE)));
        dateBytes[5] = reverseByte((byte) (calendar.get(Calendar.SECOND)));
        dateBytes[6] = reverseByte((byte) ((calendar.get(Calendar.ZONE_OFFSET) + calendar
                .get(Calendar.DST_OFFSET)) / (60 * 1000 * 15)));
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            bo.write(lsmcs);
            bo.write(scBytes);
            bo.write(0x04);
            bo.write((byte) sender.length());
            bo.write(senderBytes);
            bo.write(0x00);
            bo.write(0x00); // encoding: 0 for default 7bit
            bo.write(dateBytes);
            try {
                String sReflectedClassName = "com.android.internal.telephony.GsmAlphabet";
                Class cReflectedNFCExtras = Class.forName(sReflectedClassName);
                Method stringToGsm7BitPacked = cReflectedNFCExtras.getMethod(
                        "stringToGsm7BitPacked", new Class[] { String.class });
                stringToGsm7BitPacked.setAccessible(true);
                byte[] bodybytes = (byte[]) stringToGsm7BitPacked.invoke(null,
                        body);
                bo.write(bodybytes);
            } catch (Exception e) {
            }

            pdu = bo.toByteArray();
        } catch (IOException e) {
        }

        Intent intent = new Intent();
        intent.setClassName("com.android.mms",
                "com.android.mms.transaction.SmsReceiverService");
        intent.setAction("android.provider.Telephony.SMS_RECEIVED");
        intent.putExtra("pdus", new Object[] { pdu });
        intent.putExtra("format", "3gpp");
        intent.putExtra("sms_receiver", true);
        context.startService(intent);
    }

    private static byte reverseByte(byte b) {
        return (byte) ((b & 0xF0) >> 4 | (b & 0x0F) << 4);
    }*/
}
