package ru.javadevelop.smsfilter.controls;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * http://t-menu.ru
 * User: navff
 * Date: 14.08.12
 * Time: 18:22
 */
public class UnderlineTextView extends TextView {
    UnderlineSpan underlineSpan;
    public UnderlineTextView(Context context) {
        super(context);
    }

    public UnderlineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnderlineTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        // code to check text for null omitted
        SpannableString content = new SpannableString(text);
        underlineSpan = new UnderlineSpan();
        content.setSpan(underlineSpan, 0, text.length(), 0);
        super.setText(content, BufferType.SPANNABLE);

    }

    /**
     * Устанавливает или снимает подчёркнутость с элемента
     * @param isUnderlined
     */
    public void setUnderline(boolean isUnderlined)
    {
        SpannableString content = new SpannableString(this.getText());
        if (isUnderlined)
        {
            underlineSpan = new UnderlineSpan();
            content.setSpan(underlineSpan, 0, this.getText().length(), 0);
            super.setText(content, BufferType.SPANNABLE);
        }
        else
        {
            content.removeSpan(underlineSpan);
            super.setText(content, BufferType.SPANNABLE);
        }

    }
}
