package ru.javadevelop.smsfilter.controls;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.activities.EditFilterActivity;
import ru.javadevelop.smsfilter.common.BaseActivity;
import ru.javadevelop.smsfilter.common.ExtApplication;
import ru.javadevelop.smsfilter.common.StringUtils;
import ru.javadevelop.smsfilter.common.interfaces.IRefreshable;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;

/**
 * Created with IntelliJ IDEA.
 * User: navff
 * Date: 02.05.13
 * Time: 14:56
 * To change this template use File | Settings | File Templates.
 */
public class FilterItem extends LinearLayout {
    private Context _context;
    private LinearLayout root;
    private TextView filter_title;                  // Спам от Билайна
    private TextView filter_action;                 // Поместить в спам
    private TextView filter_sender_text;            // +79022226634
    private TextView filter_body_text;              // Привет выиграли много вкусного

    private LinearLayout details_container;         // контейнер, в котором находятся подробности фильтра
    private LinearLayout from_number_container;     // контейнер с номером телефона
    private LinearLayout words_container;           // контейнер со словами фильтра
    private LinearLayout buttons_container;         // контейнер с двумя кнопочками

    private Button  delete_button;
    private Button  edit_button;

    public BlackListFilter filter;
    public boolean expanded = false;

    //******************************************************************************************************************
    public FilterItem(Context context, BlackListFilter filter) {
        super(context);
        _context = context;
        this.filter = filter;
        init();
    }
    //******************************************************************************************************************
    public FilterItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        _context = context;
        init();
    }
    //******************************************************************************************************************
    public FilterItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        _context = context;
        init();
    }
    //******************************************************************************************************************
    private void init()
    {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li;
        li = (LayoutInflater)getContext().getSystemService(infService);
        li.inflate(R.layout.filter_item, this, true);

        findControls();
        fillControls();
        setListeners();
    }
    //******************************************************************************************************************
    private void findControls()
    {
        root = (LinearLayout)findViewById(R.id.root);
        filter_title = (TextView)findViewById(R.id.filter_title);
        filter_action = (TextView)findViewById(R.id.filter_action);
        filter_sender_text = (TextView)findViewById(R.id.filter_sender_text);
        filter_body_text = (TextView)findViewById(R.id.filter_body_text);
        details_container = (LinearLayout)findViewById(R.id.details_container);
        from_number_container = (LinearLayout)findViewById(R.id.from_number_container);
        words_container = (LinearLayout)findViewById(R.id.words_container);
        buttons_container = (LinearLayout)findViewById(R.id.buttons_container);

        edit_button = (Button)findViewById(R.id.edit_button);
        delete_button = (Button)findViewById(R.id.delete_button);
    }
    //******************************************************************************************************************
    private void fillControls()
    {
        // Заполняем заголовок. Если он пустой, — формируем его из подручных средств
        if (!StringUtils.emptyOrNull(filter.title))
        {
            filter_title.setText(filter.title);
        }
        else
        {
            filter_title.setText("[" + filter.action.toHumanWord() + "] " + filter.sender + " "
                                 +  StringUtils.getFirstWords(filter.body, 3));
        }
        filter_action.setText(filter.action.toHumanWord());
        // Заполняем номер телефона или скрываем блок, если фильтр не по номеру
        if (StringUtils.emptyOrNull(filter.sender))
        {
            from_number_container.setVisibility(GONE);
        }
        else
        {
            from_number_container.setVisibility(VISIBLE);
            filter_sender_text.setText(filter.sender);
        }
        // Заполняем слова, по которым фильтруем, если есть
        if (StringUtils.emptyOrNull(filter.body))
        {
            words_container.setVisibility(GONE);
        }
        else
        {
            words_container.setVisibility(VISIBLE);
            filter_body_text.setText(filter.body);
        }
    }
    //******************************************************************************************************************
    private void setListeners()
    {
        root.setOnClickListener(new OnFilterClick());
        delete_button.setOnClickListener(new OnDeleteClick());
        edit_button.setOnClickListener(new OnEditClick());
    }
    //******************************************************************************************************************

    /**
     * Раскрывает и закрывает текущий итем
     */
    public void expand()
    {
        if (expanded)
        {
            Drawable icon = getResources().getDrawable( R.drawable.small_arrow_down);
            filter_title.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
            details_container.setVisibility(GONE);
            expanded = false;
        }
        else
        {
            Drawable icon = getResources().getDrawable( R.drawable.small_arrow_up);
            filter_title.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
            details_container.setVisibility(VISIBLE);
            expanded = true;
        }
    }
    //******************************************************************************************************************
    private class OnFilterClick implements  OnClickListener
    {

        @Override
        public void onClick(View v) {
            expand();
        }
    }

    //******************************************************************************************************************
    private  class OnDeleteClick implements OnClickListener
    {

        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(_context)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.confirm_deletion)
                    .setMessage(R.string.confirm_filter_deletion_text)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FilterItem.this.delete();
                        }
                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        }
    }
    //******************************************************************************************************************
    public void delete()
    {
        filter.delete();
        try{
            ((IRefreshable)_context).refresh();
        }
        catch (ClassCastException e)
        {
            throw new RuntimeException("Нужно передать в качестве контекста IRefrashable");
        }
    }

    //******************************************************************************************************************
    public class OnEditClick implements OnClickListener
    {

        @Override
        public void onClick(View v) {
            ExtApplication.Session.blackListFilter = FilterItem.this.filter;
            Intent i = new Intent(_context, EditFilterActivity.class);
            _context.startActivity(i);
        }
    }
    //******************************************************************************************************************
    //******************************************************************************************************************




}

