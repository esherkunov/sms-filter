package ru.javadevelop.smsfilter.controls.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.model.filter.DeletedSms;

import java.text.DateFormat;

/**
 * User: navff
 * Date: 16.05.13
 * Time: 12:03
 */
public class FilteredSmsArrayAdapter extends ArrayAdapter<DeletedSms> {
    private static final DateFormat dateFmt = DateFormat.getDateInstance(DateFormat.SHORT);
    private static final DateFormat timeFmt = DateFormat.getTimeInstance(DateFormat.SHORT);
    private Context _context;

    //******************************************************************************************************************
    public FilteredSmsArrayAdapter(Context context, int resourceId)
    {
        super(context, resourceId, DeletedSms.getDeletedMessagesList(context));
        this._context = context;
    }
    //******************************************************************************************************************
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout v = (LinearLayout)convertView;

        if(v == null) {
            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (LinearLayout)inflater.inflate(R.layout.listview_sms_item, parent, false);
        }
        else
        {
            v.removeAllViews();
        }
        TextView sms_text = (TextView)v.findViewById(R.id.sms_text);
        TextView phone_number = (TextView)v.findViewById(R.id.phone_number);
        TextView date = (TextView)v.findViewById(R.id.date);
        DeletedSms deletedSms = DeletedSms.getDeletedMessagesList(_context).get(position);
        sms_text.setText(deletedSms.body);
        phone_number.setText(deletedSms.sender);
        date.setText(dateFmt.format(deletedSms.date)+ ' ' + timeFmt.format(deletedSms.date));
        return v;
    }
    //******************************************************************************************************************
    @Override
    public DeletedSms getItem(int position)
    {
        return DeletedSms.getDeletedMessagesList(_context).get(position);
    }
    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

}
