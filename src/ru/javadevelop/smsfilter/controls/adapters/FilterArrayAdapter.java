package ru.javadevelop.smsfilter.controls.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.controls.FilterItem;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;

import java.util.ArrayList;

/**
 * User: navff
 * Date: 21.04.13
 * Time: 12:34
 */
public class FilterArrayAdapter extends ArrayAdapter<BlackListFilter> {

    private Context _context;
    private ArrayList<BlackListFilter> _filters;
    
    public FilterArrayAdapter(Context context, int textViewResourceId, ArrayList<BlackListFilter> filters)
    {
        super(context, textViewResourceId, filters);
        this._context = context;
        this._filters  = filters;
    }
    
    //******************************************************************************************************************
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	LinearLayout v = (LinearLayout)convertView;
    	
        if(v == null) {
            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (LinearLayout)inflater.inflate(R.layout.listview_item_filter, parent, false);
        }
        else
        {
            v.removeAllViews();
        }
        // Создание FilterItem - элемента
        FilterItem  filterItem= new FilterItem(_context, _filters.get(position));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                                         LinearLayout.LayoutParams.WRAP_CONTENT);
        v.addView(filterItem);
        return v;
    }
    
    //******************************************************************************************************************
    @Override
    public BlackListFilter getItem(int position)
    {
        return  this._filters.get(position);
    }
    //******************************************************************************************************************
    public void expand (int position)
    {

    }
    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************


}
