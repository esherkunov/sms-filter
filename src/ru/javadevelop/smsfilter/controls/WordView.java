package ru.javadevelop.smsfilter.controls;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.activities.AddRuleActivity;
import ru.javadevelop.smsfilter.common.ScreenUtils;

/**
 * User: navff
 * Date: 20.04.13
 * Time: 15:13
 */
public class WordView extends LinearLayout {
    private Context _context;
    public String word;
    private TextView word_text;
    private LinearLayout root;
    public boolean isSelected = false;

    public WordView(Context context) {
        super(context);
        this._context = context;
        init();
    }

    public WordView (Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);
        this._context = context;
        init();
    }


    public WordView(Context context, String word) {
        super(context);
        this._context = context;
        this.word = word;
        init();
    }

    private void init()
    {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li;
        li = (LayoutInflater)getContext().getSystemService(infService);
        li.inflate(R.layout.view_word, this, true);
        if (this.word ==null)
        {
            this.word ="Привет!";
        }
        findControls();
        fillControls();
        setListeners();
    }
    //******************************************************************************************************************
    private void findControls()
    {
        word_text = (TextView)findViewById(R.id.word_text);
        root = (LinearLayout)findViewById(R.id.root);
    }
    //******************************************************************************************************************
    private void fillControls()
    {
        word_text.setText(this.word);
        if (word.length()>2)
        {
            setBackground();
        }

    }
    //******************************************************************************************************************
    private void setListeners()
    {
        if (word.length()>2)
        {
            root.setOnClickListener(new OnSelectListener());
        }

    }

    //******************************************************************************************************************
    public int getRealWidth()
    {
        int screenWidth = this._context.getResources().getDisplayMetrics().widthPixels;
        this.measure(screenWidth - ScreenUtils.dpToPixels(_context, AddRuleActivity.WHITE_FIELDS_WIDTH), 20);
        return  this.getMeasuredWidth();
    }
    //******************************************************************************************************************
    public class OnSelectListener implements OnClickListener
    {

        @Override
        public void onClick(View view) {
            isSelected = !isSelected;
            setBackground();
        }
    }
    //******************************************************************************************************************
    public void setWord(String newText)
    {
        this.word = newText;
        word_text.setText(this.word);
    }
    //******************************************************************************************************************
    public void setBackground()
    {
        if (isSelected)
        {
            root.setBackgroundResource(R.drawable.shape_blue_solid);
            word_text.setTextColor(Color.rgb(255, 255, 255));
        }
        else
        {
            root.setBackgroundResource(R.drawable.shape_blue_dashed);
            word_text.setTextColor(Color.rgb(0, 0, 0));
        }
    }
    //******************************************************************************************************************
}
