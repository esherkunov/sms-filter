package ru.javadevelop.smsfilter.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;

/**
 * Created with IntelliJ IDEA.
 * User: navff
 * Date: 15.05.13
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
public class ActionRadioGroupHelper {
    private Context _context;
    private TextView recipient_textview;
    public ActionRadioGroupHelper(Context context)
    {
        this._context = context;
    }

    //******************************************************************************************************************

    /**
     * Отдаёт на основании выбранного RadioButton действие, которое нужно проделать с СМС
     * @param resId  — ID выбранного RadioButton
     */
    public BlackListFilter.Action getActionFromRadiobuttonId(int resId)
    {
        switch (resId) {
            case R.id.radiobtn_create_event:
                return BlackListFilter.Action.CREATEEVENT;
            case R.id.radiobtn_delete:
                return BlackListFilter.Action.DELETEMESSAGE;
            case R.id.radiobtn_forward:
                return BlackListFilter.Action.FORWARDMESSAGE;
            case R.id.radiobtn_mark_as_read:
                return BlackListFilter.Action.MARKASREAD;
            case R.id.radiobtn_mark_as_spam:
                return BlackListFilter.Action.MARKASSPAM;

        }
        return null;
    }

    //******************************************************************************************************************

    /**
     * Устанавливает активный элемент в радиогруппе
     */
    public void setActionOnRadiobuttons(BlackListFilter.Action action, RadioGroup radio_group)
    {
        switch (action) {
            case CREATEEVENT :
                radio_group.check(R.id.radiobtn_create_event);
                break;
            case DELETEMESSAGE:
                radio_group.check(R.id.radiobtn_delete);
                break;
            case FORWARDMESSAGE:
                radio_group.check(R.id.radiobtn_forward);
                break;
            case MARKASREAD:
                radio_group.check(R.id.radiobtn_mark_as_read);
                break;
            case MARKASSPAM :
                radio_group.check(R.id.radiobtn_mark_as_spam);
                break;
        }
    }

}
