package ru.javadevelop.smsfilter.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Для полноценной работы класса нужна библиотека jodaTime
 */
public class StringUtils {
    public static  String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    //******************************************************************************************************************
    // Использование:
    // Date date = parseDate("19/05/2009", "dd/MM/yyyy");
    public static Date parseDate(String date, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }

    //******************************************************************************************************************
    public static String[] splitToWords(String str)
    {
        String delimiter = " ";
        return str.split(delimiter) ;
    }
    //******************************************************************************************************************
    public static boolean emptyOrNull(String s)
    {
        return ( (s==null) || (s.equals("")));
    }

    //******************************************************************************************************************
    
    public static String getEmptyIfNull(String s) {
    	return (s == null?"":s);
    }
    
    //******************************************************************************************************************

    /**
     * Выдаёт первые несколько слов в строке
     * @param s — строка для укорачивания
     * @param countOfWords — количество слов
     */
    public static String getFirstWords(String s, int countOfWords)
    {
        ArrayList<String> words = ArrayToArraylist(splitToWords(s)) ;
        return  getFirstWords(words, countOfWords);
    }

    //******************************************************************************************************************
    /**
     * Выдаёт первые несколько слов в строке
     * @param words — массив слов для укорачивания
     * @param countOfWords — количество слов
     */

    public static String getFirstWords(List<String> words, int countOfWords)
    {
        String result = "";
        if (countOfWords>words.size())
        {
            countOfWords = words.size();
        }
        for (int i = 0; i<countOfWords; i++)
        {
            result += words.get(i) + " ";
        }
        return result;
    }
    //******************************************************************************************************************
    private static ArrayList<String> ArrayToArraylist(String[] array)
    {
        ArrayList<String> result = new ArrayList<String>();
        Collections.addAll(result, array);
        return  result;
    }
    //******************************************************************************************************************
}
