package ru.javadevelop.smsfilter.common;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.util.Patterns;
import ru.javadevelop.smsfilter.model.Sms;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;

import java.util.regex.Pattern;


public class ExtApplication extends Application {
    private static ExtApplication instatce;
    //******************************************************************************************************************
    public static ExtApplication getInstatce()
    {
        return instatce;
    }
    //******************************************************************************************************************
    public static class Session {
        public static String searchString;
        public static Class backActivity;
        public static String googleAccount;
        public static Sms sms;
        public static BlackListFilter blackListFilter;

        //**************************************************************************************************************
        public static void clear() {
            Session.searchString = null;
            Session.backActivity = null;
        }
    }
    //******************************************************************************************************************
    @Override
    public final void onCreate()
    {
        super.onCreate();
        instatce = this;

        String dbUrl = "jdbc:h2:mem:";
        //DB.INSTANCE.openConnection(dbUrl, "sa", "sa");
        getInitialVariables();
        // setMainGoogleAccount();

        if (Session.sms==null)
        {
            Session.sms = new Sms(" ", " ", null );
        }

    }
    //******************************************************************************************************************
    @Override
    public void onTerminate() {
        super.onTerminate();
        //DB.INSTANCE.closeConnection();
    }
    //******************************************************************************************************************
    public void getInitialVariables() {
        try
        {
            AppPreferences.setDefaultParams();
            FileUtils.setSoragePlace(this);

        } catch (Exception ex)
        {
            throw new RuntimeException("Не могу получить данные");
        }
    }
    //******************************************************************************************************************
    public void setMainGoogleAccount()
    {
        Context context = getApplicationContext();
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                Session.googleAccount = account.name;
                return;
            }
        }
    }
    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************
}
