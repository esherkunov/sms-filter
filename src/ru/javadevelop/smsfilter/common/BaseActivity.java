package ru.javadevelop.smsfilter.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import ru.javadevelop.smsfilter.common.interfaces.IRefreshable;
/**
 * http://t-menu.ru
 * User: navff
 * Date: 21.07.12
 * Time: 10:36
 */

/**
 * Расширенный класс Activity с дополнительными важными методами
 */
public  class BaseActivity extends Activity {
    /**
     * Отображает окно сообщения с заданной задержкой. Не рекомендуются большие сообщения, ибо не влезут.
     * @param str   Сообщение
     * @param delay  Задержка в милисекундах
     */

    public void showMessage(String str,int delay)
    {
        Dialog dlg = new Dialog(this);
        dlg.setTitle(str);
        dlg.show();
        timerDelayRemoveDialog(delay, dlg);

    }
    //******************************************************************************************************************
    private void timerDelayRemoveDialog(long time, final Dialog d){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                d.dismiss();
            }
        }, time);
    }
    //******************************************************************************************************************
    /**
     * Устанавливает фокус ввода на заданный элемент
     * @param view   Элемент, которому передаём фокус
     */
    public  void setFocus(View view)
    {
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }
    //******************************************************************************************************************
    /**
     * Скрывает клавиатуру у заданного элемента
     * @param view Элемент, у которого скрываем клаву
     */
    public void hideKeyboard(View view)
    {
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (view==null)
        {

        }
        else
        {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    public void showKeyboard(View view)
    {
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }
    //******************************************************************************************************************

    /**
     * Обработчик ухода Avtivity с экрана. Запоминает последнюю Activity, которая была запущена
     */
    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", this.getClass().getName());
        editor.commit();
    }
}


