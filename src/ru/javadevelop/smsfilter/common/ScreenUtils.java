package ru.javadevelop.smsfilter.common;

import android.content.Context;

/**
 * http://t-menu.ru
 * User: navff
 * Date: 13.08.12
 * Time: 6:02
 */
public class ScreenUtils {
    public static int dpToPixels(Context context, int dps)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }
}
