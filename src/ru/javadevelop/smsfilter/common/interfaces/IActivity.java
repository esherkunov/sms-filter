package ru.javadevelop.smsfilter.common.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: navff
 * Date: 24.02.13
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public interface IActivity {
    public void findControls();
    public void fillControls();
    public void setListeners();
}
