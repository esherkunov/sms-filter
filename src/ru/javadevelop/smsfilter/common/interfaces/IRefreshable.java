package ru.javadevelop.smsfilter.common.interfaces;

/**
 * http://t-menu.ru
 * User: navff
 * Date: 17.08.12
 * Time: 9:14
 */
public interface IRefreshable {
    void refresh();
}
