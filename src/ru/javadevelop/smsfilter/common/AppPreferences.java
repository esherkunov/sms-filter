package ru.javadevelop.smsfilter.common;


import android.content.Context;
import android.content.SharedPreferences;

public  enum AppPreferences {
    LAST_SYNC_DATE,
    SOME_SETTING;               // При входе спрашивать не пин-код, а пароль (bool)


    public static final String PREFS_NAME = "AppPrefs";
    //******************************************************************************************************************
    //******************************************************************************************************************

    /**
     * Выводит значение параметра в Boolean
     * @return
     */
    public void clear()
    {
        Context context = getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        try {
            editor.clear();
            editor.commit();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Это не Boolean-параметр.");
        }
    }
    //******************************************************************************************************************

    /**
     * Выводит значение параметра в Boolean
     * @return
     */
    public boolean toBoolean()
    {
        try
        {
            Context context = getAppContext();
            SharedPreferences settings =  context.getSharedPreferences(PREFS_NAME, 0);
            return settings.getBoolean(this.toString(), false);
        }
        catch (Exception e)
        {
            throw  new RuntimeException("Этот параметр нельзя перевести в Boolean");
        }
    }
    //******************************************************************************************************************
    /**
     * Выводит значение параметра в String
     * @return
     */
    public String toStr()
    {
        try
        {
            Context context = getAppContext();
            SharedPreferences settings =  context.getSharedPreferences(PREFS_NAME, 0);
            return settings.getString(this.toString(), "");
        }
        catch (Exception e)
        {
            throw  new RuntimeException("Этот параметр нельзя перевести в String");
        }
    }
    //******************************************************************************************************************
    /**
     * Устанавливает значение для Boolean-параметра.
     * @param newValue
     */
    public void set(boolean  newValue)
    {
        Context context = getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        try {
            editor.putBoolean(this.toString(), newValue);
            editor.commit();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Это не Boolean-параметр.");
        }
    }
    //******************************************************************************************************************
    /**
     * Устанавливает значение для String-параметра.
     * @param newValue
     */
    public void set(String  newValue)
    {
        Context context = getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        try {
            editor.putString(this.toString(), newValue);
            editor.commit();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Это не String-параметр или вы делаете что-то не то.");
        }
    }
    //******************************************************************************************************************

    /**
     * Устанавливает значения параметров по-умолчанию.
     */
    public static void setDefaultParams()
    {
//        if ( (AppPreferences.LAST_SYNC_DATE.toStr()==null) || (AppPreferences.LAST_SYNC_DATE.toStr().equals("")) )
//        {
            AppPreferences.LAST_SYNC_DATE.set(String.valueOf(0) );
//        }

    }
    //******************************************************************************************************************
    private Context getAppContext()
    {
        return ExtApplication.getInstatce().getApplicationContext();
    }



}
