package ru.javadevelop.smsfilter.model;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;

/**
 * User: navff
 * Date: 05.05.13
 * Time: 5:42
 */
public class ContactPhone {
    public String id;
    public String displayName;
    public String phoneNumber;
    //******************************************************************************************************************

    /**
     * Получает номер телефона контакта по Uri вида:
     * ContactsContract.CommonDataKinds.Phone.CONTENT_URI
     */
    public static ArrayList<ContactPhone> getContacts(ContentResolver cr, Uri uri) {

        Cursor cur = null;
        if (uri==null)
        {
            cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        }
        else
        {
            cur = cr.query(uri, null, null, null, null);
        }
        ArrayList<ContactPhone> result = new ArrayList<ContactPhone>();
        if ( (cur!=null) && (cur.getCount() > 0) )
        {
            while (cur.moveToNext()) {
                ContactPhone contactPhone = new ContactPhone();
                // read id
                contactPhone.id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                /** read names **/
                contactPhone.displayName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                /** Phone Numbers **/
                contactPhone.phoneNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                result.add(contactPhone);
            }
        }
        return result;
    }
}
