package ru.javadevelop.smsfilter.model.filter;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SmsFilterDb extends SQLiteOpenHelper {

    public final static String TAG = "br_sms_test";
    
    private final static String DATABASE_NAME = "sms_filter.db";
    private final static int DATABASE_VERSION = 1;

    SmsFilterDb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    static {
    	cupboard().register(InnerBlackListFilter.class);
    	cupboard().register(InnerWhiteList.class);
    	cupboard().register(InnerDeletedSms.class);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    	
    	cupboard().withDatabase(db).createTables();
    	
//        String whitelist_table = "CREATE TABLE WHITELIST (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SENDER TEXT, BODY TEXT, TITLE TEXT)";
//        String blacklist_table = "CREATE TABLE BLACKLIST (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SENDER TEXT, BODY TEXT, ACTION TEXT, EXT TEXT, TITLE TEXT)";
//        String deleted_sms_table = "CREATE TABLE DELETEDSMS (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SENDER TEXT, BODY TEXT)";
//        db.execSQL(deleted_sms_table);
//        db.execSQL(whitelist_table);
//        db.execSQL(blacklist_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	
    	cupboard().withDatabase(db).upgradeTables();
    	
//		db.execSQL("DROP TABLE WHITELIST");
//	    db.execSQL("DROP TABLE BLACKLIST");
//	    db.execSQL("DROP TABLE DELETEDSMS");
//	    String whitelist_table = "CREATE TABLE WHITELIST (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SENDER TEXT, BODY TEXT, TITLE TEXT)";
//	    String blacklist_table = "CREATE TABLE BLACKLIST (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SENDER TEXT, BODY TEXT, ACTION TEXT, EXT TEXT, TITLE TEXT)";
//	    String deleted_sms_table = "CREATE TABLE DELETEDSMS (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SENDER TEXT, BODY TEXT)";
//	    db.execSQL(deleted_sms_table);
//	    db.execSQL(whitelist_table);
//	    db.execSQL(blacklist_table);
    }
}
