package ru.javadevelop.smsfilter.model.filter;

/*
POJO для таблицы DELETEDSMS. Не юзать руками. 
*/

import java.util.Calendar;

public class InnerDeletedSms {
	public Long _id;
	public String sender;
	public String body;
	public String type;
    public Calendar date;
}
