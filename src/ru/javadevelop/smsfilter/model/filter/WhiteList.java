package ru.javadevelop.smsfilter.model.filter;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import java.util.ArrayList;

import nl.qbusict.cupboard.QueryResultIterable;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class WhiteList {
	public Long _id;
	public String title;
	public String sender;
	public String body;
	
	private Context context;
	private SmsFilterDb dbHelper;
	
	public WhiteList(Context context) {
		this.context = context;
		this.dbHelper = new SmsFilterDb(this.context);
	}
	
	public long save() {
		InnerWhiteList inner = new InnerWhiteList();
		inner._id = this._id;
		inner.sender = this.sender;
		inner.body = this.body;
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Long row_id = cupboard().withDatabase(db).put(inner);
		db.close();
		return row_id;
	}
	
	public void delete() {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		cupboard().withDatabase(db).delete(InnerWhiteList.class, _id);
		db.close();
	}
	
	public static WhiteList get(Context context, long _id) {
		SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
		InnerWhiteList innerSms = cupboard()
				.withDatabase(db)
				.get(InnerWhiteList.class, _id);
		WhiteList whiteList = new WhiteList(context);
		whiteList.fillFromInner(innerSms);
		db.close();
		return whiteList;
	}
	/**
	 * Возвращает белый список
	 * @param context
	 * @return Если есть вернет список, если нет, тогда вернёт пустой список.
	 */
	public static ArrayList<WhiteList> getWhiteList(Context context) {
		ArrayList<WhiteList> whiteListArray = new ArrayList<WhiteList>();
		SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
		QueryResultIterable<InnerWhiteList> itr = cupboard().withDatabase(db).query(InnerWhiteList.class).query();
		try {
			for(InnerWhiteList inner: itr) {
				WhiteList whiteList = new WhiteList(context);
				whiteList.fillFromInner(inner);
				whiteListArray.add(whiteList);
			}
		} finally {
			itr.close();
			db.close();
		}
		return whiteListArray;
	}
	
	private void fillFromInner(InnerWhiteList innerWhiteList) {
		this._id = innerWhiteList._id;
		this.sender = innerWhiteList.sender;
		this.body = innerWhiteList.body;
	}
	
	/**
	 * Проверяет есть ли в белом списке что-то по критериям (Проверяет только по отпрвителю)
	 * @param context
	 * @param sender Номер отправителя
	 * @param body Тело смс (не проверяется, не знаю, нужно ли вообще проверять)
	 * @return true если нашел, false если нет
	 */
	public static boolean isInWhiteList(Context context, String sender, String body) {
		if(sender != null) {
			SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
			InnerWhiteList innerWhiteList = cupboard()
					.withDatabase(db)
					.query(InnerWhiteList.class)
					.withSelection("SENDER LIKE ?", sender)
					.get();
			db.close();
			if(innerWhiteList != null) {
				return true;
			}
		}
		return false;
	}
}
