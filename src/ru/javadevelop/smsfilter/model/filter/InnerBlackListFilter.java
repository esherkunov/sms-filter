package ru.javadevelop.smsfilter.model.filter;

/*
POJO для таблицы BLACKLIST. Не юзать руками. 
*/

public class InnerBlackListFilter {
	public Long _id;
	public String title;
	public String sender;
	public String body;
	public String smsBody;
	public String ext;
	public String action;
}
