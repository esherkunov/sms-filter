package ru.javadevelop.smsfilter.model.filter;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import nl.qbusict.cupboard.QueryResultIterable;

import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.common.ExtApplication;
import ru.javadevelop.smsfilter.common.StringUtils;
import ru.javadevelop.smsfilter.model.filter.DeletedSms.Type;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.telephony.SmsManager;



public class BlackListFilter {
	
	public enum Action {
		DELETEMESSAGE, MARKASREAD, FORWARDMESSAGE, MARKASSPAM, CREATEEVENT;

        /**
         * Выдаёт нормальные человеко-пронимаемые расшифровки действий. Берёт из файла ресурсов
         */
        public String toHumanWord()
        {
            Context context = ExtApplication.getInstatce().getApplicationContext();
            switch (this)  {
                case DELETEMESSAGE:
                    return  context.getString(R.string.delete);
                case MARKASREAD:
                    return  context.getString(R.string.mark_as_read_short);
                case FORWARDMESSAGE:
                    return  context.getString(R.string.forward);
                case MARKASSPAM:
                    return  context.getString(R.string.mark_as_spam_short);
                case CREATEEVENT:
                    return  context.getString(R.string.create_event_short);
            }
            return this.toString();
        }
	}
	
	public Long _id;
	public String title;
	public String sender;
	public String smsSender;
	public String body;
	public String smsBody;
	public String ext;
	public Action action;
	
	private Context context;
	private SmsFilterDb dbHelper;
	
	public BlackListFilter(Context context) {
		this.context = context;
		this.dbHelper = new SmsFilterDb(this.context);
	}
	
	/**
	 * Сохраняет себя в базу
	 * @return id записи
	 */
	public Long save() {
		InnerBlackListFilter innerBlackList = new InnerBlackListFilter();
		innerBlackList._id = this._id;
		innerBlackList.title = this.title;
		innerBlackList.sender = this.sender;
		//Меняем знаки пунктуации из строки, потом удаляем дублирующие пробелы.
        if (!StringUtils.emptyOrNull(this.body))
        {
            innerBlackList.body = this.body.replaceAll("[?:!.,;]+", " ").replaceAll("\\s+", " ");
        }
		innerBlackList.ext = this.ext;
		innerBlackList.action = this.action.toString();
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Long row_id = cupboard().withDatabase(db).put(innerBlackList);
		db.close();
		return row_id;
	}
	
	/**
	 * Удаляет себя
	 */
	public void delete() {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		cupboard().withDatabase(db).delete(InnerBlackListFilter.class, _id);
		db.close();
	}
	
	private void fillFromInner(InnerBlackListFilter innerFilter) {
		this._id = innerFilter._id;
		this.title = innerFilter.title;
		this.sender = innerFilter.sender;
		this.body = innerFilter.body;
		this.ext = innerFilter.ext;
		this.action = Action.valueOf(innerFilter.action);
	}
	
	public static BlackListFilter get(Context context, long _id) {
		SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
		InnerBlackListFilter innerBlackList = cupboard()
				.withDatabase(db)
				.get(InnerBlackListFilter.class, _id);
		db.close();
		BlackListFilter filter = new BlackListFilter(context);
		filter.fillFromInner(innerBlackList);
		return filter;
	}
	
	/**
	 * Возвращает полный список фильтров. Если список пуст, вернет пустой список.
	 * @return Полный список фильтров
	 */
	public static ArrayList<BlackListFilter> getFiltersList(Context context) {
		ArrayList<BlackListFilter> filtersList = new ArrayList<BlackListFilter>();
		SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
		QueryResultIterable<InnerBlackListFilter> itr = cupboard().withDatabase(db).query(InnerBlackListFilter.class).query();
		try {
			for(InnerBlackListFilter innerBlackList: itr) {
				BlackListFilter filterItem = new BlackListFilter(context);
				filterItem.fillFromInner(innerBlackList);
				filtersList.add(filterItem);
			}
		} finally {
			itr.close();
			db.close();
		}
		return filtersList;
	}
	
	/**
	 * Проверяет на соответсвие одному из фильтров. Сначала проверяет по отправителю, если нашли совпадения,
	 * тогда отправляем экземпляр фильтра. Если нет идём дальше проверять на соответсвие по телу смс. Совпало - 
	 * вернули фильтр, нет - null.
	 * @param context
	 * @param sender Отправитель смс, обычно номер.
	 * @param body Тело смс
	 * @return Если подходит под условия фильтра, тогда вернет этот фильтр, если нет, тогда null
	 */
	public static BlackListFilter isInBlackList(Context context, String sender, String body) {
		
		if(sender != null) {
			SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
			InnerBlackListFilter innerBlackListFilter = cupboard()
					.withDatabase(db)
					.query(InnerBlackListFilter.class)
					.withSelection("SENDER LIKE ?", sender)
					.get();
			db.close();
			if(innerBlackListFilter != null) {
				BlackListFilter blackListFilter = new BlackListFilter(context);
				blackListFilter.fillFromInner(innerBlackListFilter);
				blackListFilter.smsSender = sender;
				blackListFilter.smsBody = body;
				return blackListFilter;
			}
		}
		if(body != null) {
			SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
			QueryResultIterable<InnerBlackListFilter> itr = cupboard().withDatabase(db).query(InnerBlackListFilter.class).query();
			try {
			for(InnerBlackListFilter innerBlackListFilter: itr) {
				if(FilterUtils.matchBodies(innerBlackListFilter.body, body)) {
					BlackListFilter blackListFilter = new BlackListFilter(context);
					blackListFilter.fillFromInner(innerBlackListFilter);
					blackListFilter.smsSender = sender;
					blackListFilter.smsBody = body;
					return blackListFilter;
				}
			}
			} finally {
				itr.close();
				db.close();
			}
		}
		return null;
	}
	
	/**
	 * Запускает действие, связанное с этим фильтром
	 */
	public void executeAction() {
		switch (action) {
			case DELETEMESSAGE:
				deleteMessage();
				break;
			case CREATEEVENT:
				createCalendarEvent();
				break;
			case FORWARDMESSAGE:
				forwardMessage();
				break;
			case MARKASREAD:
				markAsRead();
				break;
			case MARKASSPAM:
				markAsSpam();
				break;
			default:
				markAsSpam();
		}
	}
	
	private void deleteMessage() {
		DeletedSms sms = new DeletedSms(context);
		sms.body = this.smsBody;
		sms.sender = this.smsSender;
		sms.type = Type.DELETED;
        sms.date = Calendar.getInstance();
		sms.save();
    }
	
    private void markAsRead() {
	    ContentValues values = new ContentValues();
	    values.put("address", smsSender);
	    values.put("body", smsBody);
	    values.put("type", 1); // inbox sms
	    values.put("date", Calendar.getInstance().getTimeInMillis());
	    values.put("seen", 1);
	    values.put("read", 1);
		context.getContentResolver().insert(Uri.parse("content://sms"), values);
    }
    
    private void forwardMessage() {
	    SmsManager smsManager = SmsManager.getDefault();
	    // TODO Описать тело пересылаемого сообщения: Пересланное сообщение от %s с текстом %s
	    smsManager.sendTextMessage(ext, null, sender + smsBody, null, null);
	    // TODO Временная мера до внедрения цепочки действий
	    markAsRead();
    }
    
    private void markAsSpam() {
    	DeletedSms sms = new DeletedSms(context);
		sms.body = this.smsBody;
		sms.sender = this.smsSender;
		sms.type = Type.SPAM;
        sms.date = Calendar.getInstance();
		sms.save();
    }
    
    private void createCalendarEvent() {
    	// TODO Сделать добавление события в календарь
    };
}
