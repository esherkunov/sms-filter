package ru.javadevelop.smsfilter.model.filter;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import nl.qbusict.cupboard.QueryResultIterable;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DeletedSms {
	
	public enum Type {
		SPAM, DELETED
	}

	public Long _id;
	public String sender;
	public String body;
	public Type type;
    public Calendar date;
	
	private Context context;
	private SmsFilterDb dbHelper;
	
	public DeletedSms(Context context) {
		this.context = context;
		this.dbHelper = new SmsFilterDb(this.context);
	}
	
	public Long save() {
		InnerDeletedSms inner = new InnerDeletedSms();
		inner._id = this._id;
		inner.sender = this.sender;
		inner.body = this.body;
		inner.type = type.toString();
        inner.date = this.date;
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Long row_id = cupboard().withDatabase(db).put(inner);
		db.close();
		return row_id;
	}
	
	public void delete() {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		cupboard().withDatabase(db).delete(InnerDeletedSms.class, _id);
		db.close();
	}
	
	public static DeletedSms get(Context context, long _id) {
		SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
		InnerDeletedSms innerSms = cupboard()
				.withDatabase(db)
				.get(InnerDeletedSms.class, _id);
		db.close();
		DeletedSms sms = new DeletedSms(context);
		sms.fillFromInner(innerSms);
		return sms;
	}
	/**
	 * Возвращает списко удаленных смс
	 * @param context
	 * @return Если есть вернет список, если нет, тогда вернёт пустой список.
	 */
	public static ArrayList<DeletedSms> getDeletedMessagesList(Context context) {
		ArrayList<DeletedSms> deletedSmsList = new ArrayList<DeletedSms>();
		SQLiteDatabase db = new SmsFilterDb(context).getReadableDatabase();
		QueryResultIterable<InnerDeletedSms> itr = cupboard().withDatabase(db).query(InnerDeletedSms.class).query();
		try {
			for(InnerDeletedSms inner: itr) {
				DeletedSms sms = new DeletedSms(context);
				sms.fillFromInner(inner);
				deletedSmsList.add(sms);
			}
		} finally {
			itr.close();
			db.close();
		}
		return deletedSmsList;
	}
	
	private void fillFromInner(InnerDeletedSms innerDeletedSms) {
		this._id = innerDeletedSms._id;
		this.sender = innerDeletedSms.sender;
		this.body = innerDeletedSms.body;
        this.date = innerDeletedSms.date;
		this.type = Type.valueOf(innerDeletedSms.type);
	}

}
