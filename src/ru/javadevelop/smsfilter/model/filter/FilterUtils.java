package ru.javadevelop.smsfilter.model.filter;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class FilterUtils {

	public static boolean matchBodies(final String pFilterBody, final String pMessageBody) {
        if(pFilterBody == null || pMessageBody == null) return false;
	    final String dividers_regex = "[- ,\\.;]";
        final String filterBody = pFilterBody.toLowerCase().trim();
        final String messageBody = pMessageBody.toLowerCase().trim();
        ArrayList<String> messageBodyArrayList = new ArrayList<String>(Arrays.asList(messageBody.split(dividers_regex)));
        ArrayList<String> filterBodyArrayList = new ArrayList<String>(Arrays.asList(filterBody.split(dividers_regex)));
        filterBodyArrayList.removeAll(messageBodyArrayList);
        if(filterBodyArrayList.size() == 0) {
            return true;
        }
        return false;
    }
	
	public static boolean isInContactList(Context context, final String sender) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(sender));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup._ID}, null, null, null);
        return (cursor.getCount() != 0);
    }

}
