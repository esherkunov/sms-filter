package ru.javadevelop.smsfilter.model;

import ru.javadevelop.smsfilter.common.StringUtils;

/**
 * User: navff
 * Date: 21.04.13
 * Time: 10:01
 */
public class Sms {
    public String phoneNumber;
    public String text;
    public String contactName;

    public Sms(String phoneNumber, String text, String contactName)
    {
        this.phoneNumber = phoneNumber;
        this.text = text;
        this.contactName = contactName;
        if (StringUtils.emptyOrNull(phoneNumber) ||
            StringUtils.emptyOrNull(text) )
        {
            throw  new RuntimeException("Sms has no number or text");
        }
    }

}
