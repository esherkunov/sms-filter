package ru.javadevelop.smsfilter.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.common.ExtApplication;
import ru.javadevelop.smsfilter.common.ScreenUtils;
import ru.javadevelop.smsfilter.common.StringUtils;
import ru.javadevelop.smsfilter.controls.UnderlineTextView;
import ru.javadevelop.smsfilter.controls.WordView;
import ru.javadevelop.smsfilter.helpers.ActionRadioGroupHelper;
import ru.javadevelop.smsfilter.model.ContactPhone;
import ru.javadevelop.smsfilter.model.Sms;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter.Action;

import java.util.ArrayList;

/**
 * User: navff
 * Date: 20.04.13
 * Time: 10:48
 */
public class AddRuleActivity extends Activity {
    private LinearLayout words_container;
    private Button next_button;
    private Button cancel_button;
    private WordView phone_number;
    private RadioGroup radio_group;
    private EditText edit_filter_name;
    private UnderlineTextView recipient_textview;
    private boolean needToAskRecipient = true;

    private Action action;
    private ArrayList<String> wordsForFilter;

    private ActionRadioGroupHelper actionRadioGroupHelper = new ActionRadioGroupHelper(this);
    private BlackListFilter filter = new BlackListFilter(this);
    private final int PICK_CONTACT = 5454545;

    public static final int WHITE_FIELDS_WIDTH = 30 ;    //  Это сумма оступов и дополнительных элементов,
                                                        //  которую нужно вычесть, чтобы получить ширину поля для воода правила


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rule);
        findControls();
        setListeners();
        fillControls();
    }

    //******************************************************************************************************************
    private void findControls()
    {

        words_container = (LinearLayout)findViewById(R.id.words_container);
        next_button = (Button)findViewById(R.id.create_button);
        cancel_button = (Button)findViewById(R.id.cancel_button);
        phone_number = (WordView)findViewById(R.id.phone_number);
        radio_group = (RadioGroup)findViewById(R.id.radio_group);
        edit_filter_name = (EditText)findViewById(R.id.edit_filter_name);
        recipient_textview = (UnderlineTextView)findViewById(R.id.recipient_textview);
    }
    //******************************************************************************************************************
    private void fillControls()
    {



        // это временно. создание тестовой смс
        if (ExtApplication.Session.sms==null)
        {
            ExtApplication.Session.sms = new Sms("+7905265654", "Жопа с метлой, подмети меня сc мостовой", null);
        }


        if (ExtApplication.Session.sms!=null)
        {
            createLines(ExtApplication.Session.sms.text);
            String contactTitle = ExtApplication.Session.sms.phoneNumber;
            if ( !StringUtils.emptyOrNull(ExtApplication.Session.sms.contactName))
            {
                contactTitle += " ("+  ExtApplication.Session.sms.contactName + ")";
            }
            phone_number.setWord(contactTitle);
        }

    }
    //******************************************************************************************************************
    private void setListeners()
    {
        next_button.setOnClickListener(new OnNextClick());
        cancel_button.setOnClickListener(new OnCancelListener());
        radio_group.setOnCheckedChangeListener(new OnActionChangedListener());
        recipient_textview.setOnClickListener(new OnForwardContactListener());
    }
    //******************************************************************************************************************

    /**
     * Собственно, конструирует контррол со словами.
     */
    private void createLines(String s)  {
        String[] words = StringUtils.splitToWords(s);

        LinearLayout line = createEmptyLine();

        int lineWidthSum = 0;
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int containerWidth = screenWidth-ScreenUtils.dpToPixels(this, WHITE_FIELDS_WIDTH);
        for (String word: words)
        {
            WordView wordView = new WordView(this, word);
            int width = wordView.getRealWidth();
            lineWidthSum += width;
            if (lineWidthSum>=containerWidth )
            {
                lineWidthSum = width;
                words_container.addView(line);
                line = createEmptyLine();
            }

            line.addView(new WordView(this, word));
        }
        if ((line.getChildCount()>0)&&(line.getParent()==null))
        {
            words_container.addView(line);
        }
    }
    //******************************************************************************************************************
    /**
     * создаёт новую пустую строку, к которую можно будет положить наши слова
     * @return
     */
    private LinearLayout createEmptyLine()
    {
        LinearLayout line = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        line.setLayoutParams(params);
        line.setOrientation(LinearLayout.HORIZONTAL);
        return  line;
    }
    //******************************************************************************************************************

    /**
     * Кнопочка «Создать»
     */
    private  class OnNextClick implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            wordsForFilter = getWordsFromControls();
            // Проверяем, правильно ли заполнены поля фильтра
            if (!validateRule(wordsForFilter, action))
            {
                return;
            }

            if(phone_number.isSelected) {
                filter.sender = phone_number.word;
            }
            if((wordsForFilter!=null) && (wordsForFilter.size()>0)) {
                filter.body = wordsListToString(wordsForFilter);
            }

            filter.action =  action;
            filter.smsSender = ExtApplication.Session.sms.phoneNumber;
            filter.smsBody = ExtApplication.Session.sms.text;
            filter.title = edit_filter_name.getText().toString() ;

            filter.save();

	        filter.executeAction();

	        Toast.makeText(getApplicationContext(), getString(R.string.rule_added), Toast.LENGTH_LONG).show();

            AddRuleActivity.this.finish();

        }
    }
    //******************************************************************************************************************

    /**
     * Создаёт из списка слов одну строку со словами через пробел
     */
    private  String wordsListToString(ArrayList<String> words)
    {
        String result = "" ;
        for (String s: words)
        {
            if (result.equals(""))
            {
                result = s;
            }
            else
            {
                result = result + " " + s;
            }
        }
        return result;
    }

    //******************************************************************************************************************

    /**
     * Извлекает из контрола со словами собственно, слова
     */
    private ArrayList<String> getWordsFromControls()
    {
        ArrayList<LinearLayout> lines = new ArrayList<LinearLayout>();
        ArrayList<WordView> wordViews = new ArrayList<WordView>();
        ArrayList<String> result = new ArrayList<String>();
        //  сначала добываем все строки
        for (int i=0; i<words_container.getChildCount(); i++)
        {
            lines.add((LinearLayout)words_container.getChildAt(i));
        }
        // потом, проходя по каждой строке берём её детей. Это WordView
        if ( (lines!=null)  && (lines.size()>0))
        {
            for (LinearLayout line : lines)
            {
                for (int j=0; j<line.getChildCount(); j++)
                {
                    wordViews.add((WordView)line.getChildAt(j));
                }
            }
        }
        // проходим по полученным WordView и забираем у активных слова
        for (WordView wordView : wordViews)
        {
            if (wordView.isSelected)
            {
                result.add(wordView.word);
            }
        }
        return result;

    }
    //******************************************************************************************************************

    /**
     * Отдаёт на основании выбранного RadioButton действие, которое нужно проделать с СМС
     * @param resId  — ID выбранного RadioButton
     */
    private Action getActionFromRadiobuttonId(int resId)
    {
        switch (resId) {
            case R.id.radiobtn_create_event:
               return Action.CREATEEVENT;
            case R.id.radiobtn_delete:
                return Action.DELETEMESSAGE;
            case R.id.radiobtn_forward:
                return Action.FORWARDMESSAGE;
            case R.id.radiobtn_mark_as_read:
                return Action.MARKASREAD;
            case R.id.radiobtn_mark_as_spam:
                return Action.MARKASSPAM;

        }
        return null;
    }
    //******************************************************************************************************************

    /**
     *  Проверяет, валидно ли правило. Все ли нужные поля заполнены
     * @param wordsForFilter    — список слов для фильтрации
     * @param action — действие
     * @return
     */
    public boolean validateRule(ArrayList<String> wordsForFilter, Action action)
    {
        if ( (!phone_number.isSelected) &&
             ((wordsForFilter==null)|| (wordsForFilter.size()==0) ) )
        {
            Toast.makeText(getApplicationContext(),
                           getString(R.string.not_entered_phone_or_words),
                           Toast.LENGTH_SHORT).show();
            return false;
        }
        if (action == null)
        {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.not_selected_action),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    //******************************************************************************************************************

    /**
     * Кнопка «Отмена»
     */
    public class OnCancelListener implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
        	BlackListFilter filter = new BlackListFilter(getApplicationContext());
        	filter.action = BlackListFilter.Action.MARKASREAD;
        	filter.smsBody = ExtApplication.Session.sms.text;
        	filter.smsSender = ExtApplication.Session.sms.phoneNumber;
        	filter.executeAction();
            AddRuleActivity.this.finish();
        }
    }
    //******************************************************************************************************************

    /**
     * Реакция на выбор Action'а
     */
    public  class OnActionChangedListener implements RadioGroup.OnCheckedChangeListener
    {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            AddRuleActivity.this.action = getActionFromRadiobuttonId(radio_group.getCheckedRadioButtonId());
            String captionText = "[" + AddRuleActivity.this.action.toHumanWord() + "] ";
            wordsForFilter = getWordsFromControls();
            if (phone_number.isSelected)
            {
                captionText += phone_number.word + "; ";
            }
            if ( (wordsForFilter!=null) &&  (!wordsForFilter.isEmpty()) )
            {
                String words =  StringUtils.getFirstWords(wordsForFilter , 5);
                captionText += words;
            }
            edit_filter_name.setText(captionText);

            //**********
            // Проверяем, выбрано ли действие FORWARD  и стартуем активити для выбора контакта
            if (   (getActionFromRadiobuttonId(checkedId) ==
                    BlackListFilter.Action.FORWARDMESSAGE ) &&
                    (needToAskRecipient))
            {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
            needToAskRecipient = true;
            // Отключаем вывод имени контакта для пересылки, если не выбрано действие «переслать»
            if ((getActionFromRadiobuttonId(checkedId) !=
                    BlackListFilter.Action.FORWARDMESSAGE ))
            {
                recipient_textview.setVisibility(View.GONE);
            }
        }
    }
    //******************************************************************************************************************
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (PICK_CONTACT) :
            {
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    ContentResolver cr = getContentResolver();
                    ContactPhone contactPhone = ContactPhone.getContacts(cr, uri).get(0);
                    filter.ext = contactPhone.phoneNumber;
                    recipient_textview.setText(contactPhone.displayName);
                    recipient_textview.setVisibility(View.VISIBLE);
                }
                else
                {
                    needToAskRecipient = false;
                    actionRadioGroupHelper.setActionOnRadiobuttons(filter.action, radio_group);
                    if (filter.action!= BlackListFilter.Action.FORWARDMESSAGE)
                    {
                        recipient_textview.setVisibility(View.GONE);
                    }
                    else
                    {
                        recipient_textview.setVisibility(View.VISIBLE);
                    }
                }
                break;
            }
        }
    }

    //******************************************************************************************************************
    public class OnForwardContactListener implements View.OnClickListener
    {

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        }
    }
    //******************************************************************************************************************
    //******************************************************************************************************************
}
