package ru.javadevelop.smsfilter.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.common.interfaces.IRefreshable;
import ru.javadevelop.smsfilter.controls.adapters.FilterArrayAdapter;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter.Action;
import ru.javadevelop.smsfilter.model.filter.DeletedSms;

import java.util.ArrayList;

public class ListFiltersActivity extends ListActivity implements IRefreshable{
    Button addRuleButton;
    int expandedItem = 9999;
    FilterArrayAdapter arrayAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_filters);
        findControls();
        refresh();
    }
    //******************************************************************************************************************
    private void findControls()
    {
       addRuleButton = (Button)findViewById(R.id.add_rule_button);
    }
    //******************************************************************************************************************
    private void fillControls()
    {
        ArrayList<BlackListFilter> filters = BlackListFilter.getFiltersList(this);
    	
        arrayAdapter = new FilterArrayAdapter(this, R.layout.listview_item_filter, filters);
        setListAdapter(arrayAdapter);
    }
    //******************************************************************************************************************
    private void setListeners()
    {
         addRuleButton.setOnClickListener(new AddRuleListener());
    }

    @Override
    public void refresh() {
        fillControls();
        setListeners();
    }

    //******************************************************************************************************************
    private class AddRuleListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            Intent i = new Intent(ListFiltersActivity.this, AddRuleActivity.class);
            startActivity(i);
            // ListFiltersActivity.this.finish();
        }
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
//    	Если активити всё еще висит в памяти, но были добавлены новые данные
    	if(arrayAdapter != null) {
    		arrayAdapter.notifyDataSetChanged();
    	}
        this.refresh();
    }
    //******************************************************************************************************************
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
    }
    //******************************************************************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    //******************************************************************************************************************
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_settings:
                openSettings();
                return true;
            case R.id.menu_list_filtered_sms:
                openListFilteredSms();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //******************************************************************************************************************

    /**
     * Реакция на нажатие пункта меню «Настройки»
     */
    private void openSettings()
    {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }
    //******************************************************************************************************************

    private  void openListFilteredSms()
    {
        Intent i = new Intent(this, ListFilteredSmsActivity.class);
        startActivity(i);
    }
}
