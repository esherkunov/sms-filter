package ru.javadevelop.smsfilter.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.common.ExtApplication;
import ru.javadevelop.smsfilter.common.StringUtils;
import ru.javadevelop.smsfilter.controls.UnderlineTextView;
import ru.javadevelop.smsfilter.helpers.ActionRadioGroupHelper;
import ru.javadevelop.smsfilter.model.ContactPhone;
import ru.javadevelop.smsfilter.model.filter.BlackListFilter;

/**
 * User: navff
 * Date: 03.05.13
 * Time: 6:45
 */
public class EditFilterActivity extends Activity {
    private EditText edit_filter_name;
    private EditText edit_filter_phone;
    private EditText edit_filter_words;
    private RadioGroup radio_group;
    private Button save_button;
    private  Button cancel_button;
    private UnderlineTextView recipient_textview;
    private TextView activity_caption;
    private boolean needToAskRecipient = true;
    private ActionRadioGroupHelper actionRadioGroupHelper = new ActionRadioGroupHelper(this);

    private final int PICK_CONTACT = 1565149;

    private BlackListFilter filter = ExtApplication.Session.blackListFilter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_rule);
        findControls();
        fillControls();
        setListeners();
    }
    //******************************************************************************************************************
    private void findControls()
    {
        edit_filter_name = (EditText)findViewById(R.id.edit_filter_name);
        edit_filter_phone = (EditText)findViewById(R.id.edit_filter_phone);
        edit_filter_words = (EditText)findViewById(R.id.edit_filter_words);
        radio_group = (RadioGroup)findViewById(R.id.radio_group);
        save_button = (Button)findViewById(R.id.save_button);
        cancel_button = (Button)findViewById(R.id.cancel_button);
        activity_caption = (TextView)findViewById(R.id.activity_caption);
        recipient_textview = (UnderlineTextView)findViewById(R.id.recipient_textview);
    }
    //******************************************************************************************************************
    private void setListeners()
    {
        save_button.setOnClickListener(new OnSaveClick());
        radio_group.setOnCheckedChangeListener(new OnActionChangedListener());
        cancel_button.setOnClickListener(new OnCancelClick());
        recipient_textview.setOnClickListener(new OnForwardContactListener());
    }
    //******************************************************************************************************************
    private void fillControls()
    {
        if (filter == null)
        {
            filter = new BlackListFilter(this);
            activity_caption.setText(getText(R.string.edit_filter));
            return;
        }

        // Показываем получателя, если  действие — переслать
        if (filter.action == BlackListFilter.Action.FORWARDMESSAGE )
        {
            // Если не заполнено, куда пересылать (filter.ext), —
            // значит действие изменилось и нужно будет спросить, кому пересылать
            if (StringUtils.emptyOrNull(filter.ext))
            {
                save_button.setText(R.string.next);
                needToAskRecipient = true;
            }
            else
            {
                recipient_textview.setText(filter.ext);     // TODO: [Жене] тут нужно на основании номера поучить имя контакта
            }
            recipient_textview.setVisibility(View.VISIBLE);
        }
        else
        {
            recipient_textview.setVisibility(View.GONE);
        }

        edit_filter_name.setText(filter.title);
        edit_filter_phone.setText(filter.sender);
        edit_filter_words.setText(filter.body);
        actionRadioGroupHelper.setActionOnRadiobuttons(filter.action, radio_group);
    }




    //******************************************************************************************************************
    public class OnCancelClick implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            EditFilterActivity.this.finish();

        }
    }
    //******************************************************************************************************************
    public class OnSaveClick implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            filter.action = actionRadioGroupHelper.getActionFromRadiobuttonId(radio_group.getCheckedRadioButtonId());
            filter.body = edit_filter_words.getText().toString();
            filter.sender = edit_filter_phone.getText().toString();
            filter.title = edit_filter_name.getText().toString();
            filter.save();
            Intent i = new Intent(EditFilterActivity.this, ListFiltersActivity.class);
            startActivity(i);
            EditFilterActivity.this.finish();

        }
    }
    //******************************************************************************************************************
    public  class OnActionChangedListener implements RadioGroup.OnCheckedChangeListener
    {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId)
        {

            // Проверяем, выбрано ли действие FORWARD  и стартуем активити для выбора контакта
            if (   (actionRadioGroupHelper.getActionFromRadiobuttonId(checkedId) ==
                    BlackListFilter.Action.FORWARDMESSAGE ) &&
                    (needToAskRecipient))
            {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
            needToAskRecipient = true;
            // Отключаем вывод имени контакта для пересылки, если не выбрано действие «переслать»
            if ((actionRadioGroupHelper.getActionFromRadiobuttonId(checkedId) !=
                    BlackListFilter.Action.FORWARDMESSAGE ))
            {
                recipient_textview.setVisibility(View.GONE);
            }
        }
    }
    //******************************************************************************************************************
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (PICK_CONTACT) :
            {
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    ContentResolver cr = getContentResolver();
                    ContactPhone contactPhone = ContactPhone.getContacts(cr, uri).get(0);
                    filter.ext = contactPhone.phoneNumber;
                    recipient_textview.setText(contactPhone.displayName);
                    recipient_textview.setVisibility(View.VISIBLE);
                }
                else
                {
                    needToAskRecipient = false;
                    actionRadioGroupHelper.setActionOnRadiobuttons(filter.action, radio_group);
                    if (filter.action!= BlackListFilter.Action.FORWARDMESSAGE)
                    {
                        recipient_textview.setVisibility(View.GONE);
                    }
                    else
                    {
                        recipient_textview.setVisibility(View.VISIBLE);
                    }
                }
                break;
            }
        }
    }
    //******************************************************************************************************************
    //******************************************************************************************************************
    public class OnForwardContactListener implements View.OnClickListener
    {

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        }
    }
    //******************************************************************************************************************
    //******************************************************************************************************************
}
