package ru.javadevelop.smsfilter.activities;

import android.app.ListActivity;
import android.os.Bundle;
import ru.javadevelop.smsfilter.R;
import ru.javadevelop.smsfilter.controls.adapters.FilteredSmsArrayAdapter;

/**
 * User: navff
 * Date: 16.05.13
 * Time: 10:43
 */
public class ListFilteredSmsActivity extends ListActivity {

    FilteredSmsArrayAdapter arrayAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_filtered_sms);
        findControls();
        fillControls();
        setListeners();
    }
    //******************************************************************************************************************
    private void findControls()
    {

    }
    //******************************************************************************************************************
    private void fillControls()
    {
        arrayAdapter = new FilteredSmsArrayAdapter(this, R.layout.listview_sms_item);
        setListAdapter(arrayAdapter);
    }
    //******************************************************************************************************************
    private void setListeners()
    {

    }
}
